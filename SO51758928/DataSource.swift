//
//  DataSource.swift
//  SO51758928
//
//  Created by Abizer Nasir on 09/08/2018.
//  Copyright © 2018 Jungle Candy Software Ltd. All rights reserved.
//

import Foundation

// Define objects to display in the table
enum Value {
    case header
    case value(String)

    // Just a rough set of values to display in the table
    static func all() -> [Value] {
        return [
            .header,
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("First"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("Second"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("Third"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0"),
            .value("0")
        ]
    }
}
