//
//  SelectionCell.swift
//  SO51758928
//
//  Created by Abizer Nasir on 09/08/2018.
//  Copyright © 2018 Jungle Candy Software Ltd. All rights reserved.
//

import UIKit

class SelectionCell: UITableViewCell {
    enum Button {
        case first
        case second
        case third
    }

    var callback: ((Button) -> Void)?

    @IBAction func firstButtonSelected(_ sender: UIButton) {
        callback?(.first)
    }

    @IBAction func secondButtonSelected(_ sender: UIButton) {
        callback?(.second)
    }

    @IBAction func thirdButtonSelected(_ sender: UIButton) {
        callback?(.third)
    }
}
