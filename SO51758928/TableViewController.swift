//
//  TableViewController.swift
//  SO51758928
//
//  Created by Abizer Nasir on 09/08/2018.
//  Copyright © 2018 Jungle Candy Software Ltd. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    let values = Value.all()

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let value = values[indexPath.row]
        switch value {
        case .header:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Selection", for: indexPath) as! SelectionCell
            cell.callback = handler
            return cell
        case .value:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Display", for: indexPath) as! DisplayCell
            cell.configure(with: value)
            return cell
        }
    }

    private func handler(button: SelectionCell.Button) -> Void {
        print("selected \(button)")
        let index: Int?
        switch button {
        case .first:
            index = values.index(where: { (value) -> Bool in
                if case .value("First") = value {
                return true
                }
                return false
            })

        case .second:
            index = values.index(where: { (value) -> Bool in
                if case .value("Second") = value {
                    return true
                }
                return false
            })
        case .third:
            index = values.index(where: { (value) -> Bool in
                if case .value("Third") = value {
                    return true
                }
                return false
            })
        }
        guard let row = index else { return }
        let indexPath = IndexPath(row: row, section: 0)
        
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}
