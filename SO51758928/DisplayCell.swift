//
//  DisplayCell.swift
//  SO51758928
//
//  Created by Abizer Nasir on 09/08/2018.
//  Copyright © 2018 Jungle Candy Software Ltd. All rights reserved.
//

import UIKit

class DisplayCell: UITableViewCell {
    @IBOutlet private var label: UILabel!

    func configure(with value: Value) {
        switch value {
        case .value(let string):
            label.text = string
        case .header:
            label.text = ""
        }
    }
}
