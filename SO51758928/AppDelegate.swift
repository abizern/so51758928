//
//  AppDelegate.swift
//  SO51758928
//
//  Created by Abizer Nasir on 09/08/2018.
//  Copyright © 2018 Jungle Candy Software Ltd. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

